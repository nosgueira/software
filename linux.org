#+TITLE: Linux

* Sistema de Arquivos

- =/bin=: Contém arquivos de programas do sistema que serão utilizados com frequência pelo usuário;
- =/boot=: Contém arquivos necessários para a inicialização do sistema;
- =/cdroom=: ponto de montagem da unidade CD-ROM
- =/media=: ponto de montagem de dispositivos diversos do sistema (rede, pendrives, CD-ROM em distribuições mais novas);
- =/ect=: arquivos de configuração do seu computador local;
- =/floppy=: ponto de montagem de unidades de disquetes;
- =/home=: diretórios contendo arquivos dos usuários;
- =/lib=: bibliotecas compartilhadas pelos programas do sistema e módulos do kernel;
- =/lost+:found= local para a gravação de arquivos/diretórios recuperados pelo utilitário **fsck.ext2**. Cada partição possui seu próp    rio diretório =lost+found=;
- =/mnt=: ponto de montagem temporário;
- =/proc=: Sistema de arquivos do kernel. Esse diretório não existe em seu disco rígido, ele é colocado lá pelo kernel e usado por div    ersos programas que fazem sua leitura, verificam configurações do sistema ou  modificar o funcionamento de dispositivos do sistema a    través da alteração em seus arquivos;
- =/sys=: Sistema de arquivos do kernel. Esse diretório não existe em seu disco rígido, ele é colocado lá pelo kernel e usado por dive    rsos programas que fazem sua leitura, verificam configurações do sistema ou  modificar o funcionamento de dispositivos do sistema at    ravés da alteração em seus arquivos;
- =/root=: diretório do usuário =root=;
- =/sbin=: diretório de programas utilizados pelo superusuário (  =root= ) para administração e controle do sistema;
- =/tmp=: diretório para armazenamento de arquivos temporários criados por programas;
- =/usr=: contém a maior parte de seus programas. Normalmente acessível somente como leitura;
- =/var=: contém maior parte dos arquivos que são gravados com frequência pelos programas do sistema, e-mails, spool de impressora, ca    che, etc;
  
* Package Managers
** pacman 
**** Commands

| flag        | description                                                                 |
| ----------- | --------------------------------------------------------------------------- |
| -S [name]   | install                                                                     |
| -Sy         | sync databases if you haven't sync them recently (equivalent to apt update) |
| -Syy        | sync databases even if you have sync them recently                          |
| -Su         | equivalent to apt upgrade                                                   |
| -Syu        | updates repository and upgrade your packages                                |
| -Ss [regex] | search                                                                      |
| -R [name]   | remove programs                                                             |
| -Rs [name]  | remove programs and their dependencies that are no more needed              |
| -Rn [name]  | remove configuration files to                                               |
| -Rns[name]  | executes the three options above at once                                    |
| -Q          | list out every single package you have installed on your computer           |
| -Qe         | list programms explicity installed by you or by some other program          |
| -q          | only provides program name, not the version                                 |
| -Qn         | will list all the programs installed from main repos                        |
| -Qm         | will list all the programs installed from aur                               |
| -Qdt        | list dependencies that aren't needed anymore                                |

**** Files
- /etc/pacman.d/mirrorlist (contains the mirror list)
- /ect/pacman.conf (configuration file)

* Core Utils
** sed
*** Flags

- i : operate on the given file

*** Pattern Matching

#+begin_src shell
  sed 'Replace/s/the/THE/g'
#+end_src

*MEANNING*: In every line that has the word "Replace" do the substitution

*** Deleting lines

#+begin_src shell
  sed '/line_patternn/d' filename
#+end_src

*MEANNING*: Delete lines matching the line pattern

*** Multiple commands

#+begin_src shell
  sed -e 's/usr/u/g' -e 's/bin/b/g'
#+end_src

*** Using other separators

Sed can infeer what is a separator

#+begin_src shell
  sed -e 's|usr|u|g' -e 's#bin#b#g'
#+end_src

*** Using sed as grep

#+begin_src shell
  sed -n '/#/p' /etc/shells
#+end_src
** cut

#+begin_src shell
  echo "This is a line of text!" | cut -c 1-10
#+end_src

#+RESULTS:
: This is a

#+begin_src shell
  cut -c 1-10 ~/.xinitrc
#+end_src

#+RESULTS:
| /home/gabr |       |
| xrdb       | -load |
| /home/gabr |       |
| flameshot  |       |
| clipmenud  |       |
| xsetroot   | -     |
| dunst      | &     |
| ./.fehbg   | &     |
| picom      | &     |
| emacs      | --da  |
| megasync   | &     |
| setxkbmap  |       |
| exec       | xmona |

#+begin_src shell
  cut -c 5- ~/.xinitrc
#+end_src

#+RESULTS:
| e/gabriel/.local/bin/dwmbar.sh      | &                       |         |         |    |          |      |         |             |
| -load                               | /home/gabriel/.Xresources | &       |         |    |          |      |         |             |
| e/gabriel/.screenlayout/monitors.sh |                         |         |         |    |          |      |         |             |
| eshot                               | &                       |         |         |    |          |      |         |             |
| menud                               | &                       |         |         |    |          |      |         |             |
| root                                | -cursor_name             | left_ptr | &       |    |          |      |         |             |
| t                                   | &                       |         |         |    |          |      |         |             |
| ehbg                                | &                       |         |         |    |          |      |         |             |
| m                                   | &                       |         |         |    |          |      |         |             |
| s                                   | --daemon&               |         |         |    |          |      |         |             |
| sync                                | &                       |         |         |    |          |      |         |             |
| kbmap                               | -model                  | abnt    | -layout | us | -variant | intl | -option | ctrl:nocaps |
| xmonad                              |                         |         |         |    |          |      |         |             |

#+begin_src shell
  echo "This is a line of text" | cut -d ' ' -f5
#+end_src

#+RESULTS:
: of
** tr

#+begin_src shell
  echo "This is a line of text" | tr 'a' 'A'
#+end_src

#+RESULTS:
: This is A line of text

#+begin_src shell
  echo "This is a line of text" | tr 'aeiou' 'AEIOU'
#+end_src

#+RESULTS:
: ThIs Is A lInE Of tExt

#+begin_src shell
  echo "This is a line of text" | tr -d 'aeiou'
#+end_src

#+RESULTS:
: Ths s  ln f txt

#+begin_src shell
  echo "Thiiiis iis    aaaaa    liiineeeee      of teeeext" | tr -s 'aeiou '
#+end_src

#+RESULTS:
: This is a line of text

#+begin_src shell
  echo "Thiiiis iis    aaaaa    liiineeeee      of teeeext" |
      tr -s '[:lower:]' '[:upper:]'
#+end_src

#+RESULTS:
: THIS IS    A    LINE      OF TEXT

#+begin_src shell
  echo "This is a line of text" | tr -cd 'aeiou'
#+end_src

#+RESULTS:
: iiaieoe
